const carDetails = require("./inventory")

function yearLog(){
    let yearList = [];
    for(let i=0; i<carDetails.length; i++){
        yearList.push(carDetails[i].car_year);
    }
    return yearList;
}

module.exports = yearLog;