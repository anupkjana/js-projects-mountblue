const findBmwandAudi = require("../problem6")

const result =findBmwandAudi()
// console.log(result)
console.log(JSON.stringify(result,null,2))  //JSON.stringify(value, replacer, space)

//value= which value/object we want to make JSON string

//replacer =this parameter is set to null, which means that no special filtering or 
//transformation of properties is applied during the stringification

//space= this parameter is set to 2, indicating that there should be 
//two spaces of indentation for each level of nesting in the resulting JSON string
