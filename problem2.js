const carDetails = require('./inventory');

function lasCartEntry(){
    if(carDetails.length>0){
        const lastCarDetails= carDetails[carDetails.length-1]
        return `Last car is a ${lastCarDetails.car_make} ${lastCarDetails.car_model}`
    } else{
        return null
    }
}

module.exports =lasCartEntry;