const carDetails = require('./inventory')


function sortByCarModels(){
    let sortedCarModels=[];
    for(let i=0; i<carDetails.length;i++){
        sortedCarModels.push(carDetails[i].car_model)
    }
    return sortedCarModels.sort()
}

module.exports = sortByCarModels;
