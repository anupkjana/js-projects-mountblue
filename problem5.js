const carDetails = require("./inventory")

function countOlderCars(){
    let counter=0
    for(let i=0; i<carDetails.length; i++){
        if(carDetails[i].car_year<2000){
            counter+=1
        }
    }
    return counter
}

module.exports= countOlderCars