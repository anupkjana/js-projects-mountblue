//The dealer can't recall the information for a car with an id of 33 on his lot. 
//Help the dealer find out which car has an id of 33 by calling a function 
//that will return the data for that car. Then log the car's year, make, and model 
//in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
const carDetails= require("./inventory");

function searchCarById(carID){
    for(let i=0; i<carDetails.length; i++){
        if(carDetails[i].id===carID){
            // let inventory[i] = inventory[i];
            return `Car ${carID} is a ${carDetails[i].car_year} ${carDetails[i].car_make} ${carDetails[i].car_model}`
        }
    }
    return null;
}
module.exports =searchCarById;
// console.log(searchCarById(33));