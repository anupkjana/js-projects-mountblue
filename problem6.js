const carDetails = require("./inventory")
// console.log(carDetails)

function findBmwandAudi(){
    let bmwAndAudi=[];
    for(let i=0; i<carDetails.length; i++){
        if(carDetails[i].car_make==="BMW" || carDetails[i].car_make==="Audi"){
            bmwAndAudi.push(carDetails[i]);
        }
    }
    return bmwAndAudi
}

module.exports =findBmwandAudi;